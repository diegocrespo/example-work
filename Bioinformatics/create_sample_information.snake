workdir: "../../samples"


rule all:
  input:
      "fastq_symlinks.done",
      "../path/to/sample_info.csv"

rule create_sample_info:
    input:
        hd_brains = "../path/to/file.csv"
    output:
         out_df = "../path/to/sample_info.csv"
    run:
        import pandas as pd
        import subprocess
        from collections import defaultdict
        import os

        #read in the xlsx with all the sample data
        hd_info = pd.read_csv(input.hd_brains, sep=',')
        msr_id = hd_info["MSR ID"].tolist()
        msr_fix =[]
        hd_info = hd_info.rename(index=str, columns={"RIN": "mRNASeq.RIN"
                                                     ,"Striatal":"HDSubject.striatal_score"
                                                     ,"Cortical":"HDSubject.cortical_score"
                                                     ,"Sex":"Subject.sex"
                                                     ,"grade":"HDSubject.grade"
                                                     ,"PMI":"Subject.PMI"
                                                     ,"Death":"Subject.death"
                                                     ,"Onset":"HDSubject.onset"
                                                     ,"CAG":"HDSubject.CAG"
                                                     ,"Batch": "Dataset.batch"
                                                     ,"Brain":"Subject.external_id"
                                                     ,"BU_ID":"Subject.subject_id"})

        # fix the names of in input file to match sample names
        for i in msr_id:
            split = i.split("_")
            new_id = ("-").join([split[0], split[1]])
            msr_fix.append(new_id)

        num_rows = len(hd_info["Subject.PMI"].tolist())

        hd_info["fix_id"] = msr_fix
        hd_info["Subject.subject_type"] = ["HD"] * num_rows
        hd_info["estimated onset"] = ["NaN"] * num_rows
        hd_info["Dataset.protocol"] = ["Ribo-depleted"] * num_rows
        hd_info["mRNASeq.readlen"] =  ["75"] * num_rows
        hd_info["mRNASeq.read1_numreads"] = ["NaN"] * num_rows
        hd_info["mRNASeq.read2_numreads"] = ["NaN"] * num_rows
        hd_info["Dataset.datatype"] = ["mRNASeq"] * num_rows

        call = "ls /path/to/source/files/2018*B{1..5}/RM*.fastq.gz > all.txt"
        subprocess.call(call, shell=True)
        samples = []
        with open("all.txt", "r") as all_samp:
            for line in all_samp:
                if "RM-CN" in line:
                    print("removing line")
                    pass
                else:
                    samples.append(line)
        os.remove("all.txt")
        
        clean_samples = [_.strip("\n") for _ in samples]

        samples_dict = {}

        for sample in clean_samples:
            real_path = os.path.realpath(sample)
            dir_path = os.path.dirname(sample)            
            base_name = os.path.basename(sample)

            #["RM-####", "S#", "L###", "R#", "###", "###.fastq.gz"]            
            split = base_name.rsplit("_", -1)
            name = split[0]            
            
            if split[3] == 'R1':
                read1_path = real_path
                #replace R1 with R2                
                split[3] = 'R2'
                read2_path = os.path.join(dir_path, "_".join(split))

            elif split[3] == 'R2':
                read2_path = real_path
                split[3] = 'R1'
                read1_path = os.path.join(dir_path, "_".join(split))
                
            #recreate basename with R2 then join to directory path

            if name not in samples_dict:
                #create dictionary with only the unique info for each sample
                samples_dict[name] = {"sample_name": name
                                      ,"mRNASeq.read1_path": read1_path
                                      ,"mRNASeq.read2_path": read2_path}
            else:
                pass
        uniq_df = pd.DataFrame.from_dict(samples_dict, orient="index")
        
        merge_df = pd.merge(hd_info, uniq_df, how="inner", left_on=["fix_id"]
                            ,right_on=["sample_name"])


        columns = ["Subject.subject_id", "Subject.subject_type"
                   ,"Subject.external_id"
                   ,"Subject.PMI", "Subject.death","Subject.sex"
                   ,"HDSubject.CAG", "HDSubject.onset", "HDSubject.grade"
                   ,"HDSubject.striatal_score"
                   ,"HDSubject.cortical_score", "HDSubject.cag_adj_onset"
                   ,"HDSubject.cag_adj_striatal_score"
                   ,"HDSubject.cag_adj_cortical_score"
                   ,"Dataset.dataset_id","Dataset.datatype"
                   ,"Dataset.protocol"
                   ,"Dataset.batch","mRNASeq.readlen", "mRNASeq.read1_path"
                   ,"mRNASeq.read1_numreads", "mRNASeq.read2_path"
                   ,"mRNASeq.read2_numreads", "mRNASeq.RIN"]

        #H_ names 
        Subject_subject_id = merge_df["Subject.subject_id"]

        # #RM- names
        subject_id = merge_df["fix_id"].tolist()

        #creating dataset_id column
        mrnaseq = ["BA9_mRNASeq_ribo"] * len(Subject_subject_id)
        
        # join H_ with BA9_mRNASeq_ribo
        hd_mrna = ["_".join([a,b]) for (a,b) in zip(Subject_subject_id, mrnaseq)]
        merge_df["Dataset.dataset_id"] = hd_mrna

        output_df =  merge_df[columns]
        num_samples = int(len(clean_samples)/16)
        assert output_df.shape[0] == num_samples, "{} does not equal {} rows in df".format(num_samples, output_df.shape[0])
        output_df.to_csv(output.out_df, sep=",", index=False)

rule create_symlink:
    input:
        in_df = "../path/to/sample_info.csv"
    output:
        touch("fastq_symlinks.done")
    run:
        import os
        import pandas as pd
        import re
        
        #checking to make sure all files will create symlinks
        # ouput is truncated in shell so redirect to txt file
        df = pd.read_csv(input.in_df, sep=",")

        # tuple contains path to folder with files and H_name
        rm_dict = dict((x, y) for x, y in zip(df["mRNASeq.read1_path"],df["Dataset.dataset_id"].tolist()))
        for sample_path,h_name in rm_dict.items():
            dir_path = os.path.dirname(sample_path)
            base_name = os.path.basename(sample_path)
            #["RM-####", "S#", "L###", "R#", "###", "###.fastq.gz"]            
            split = base_name.rsplit("_", -1)
            name = split[0]
            #/path/to/sample/{sample_name}_(S\d+)_L(\d\d\d)_R[12]_001_(\d\d\d).fastq.gz
            full_regex = "_".join([name, "(S\d+)_L(\d\d\d)_R[12]_001_(\d\d\d).fastq.gz"])            
            pattern = re.compile(full_regex)
            #get all files in directory
            files = [
                _ for dirpath,dirnames,filenames in os.walk(dir_path) for _ in filenames]
        
            # list of 16 path names
            samples = list(filter(pattern.match, files))
            assert len(samples) == 16, print(samples)
            
            # get ###.fastq.gz
            ids = [_.rsplit("_", -1)[5].split(".")[0] for _ in samples]
            uniq_ids = set(ids)
            # Create H_ names for the 16 samples
            for i in samples:
                file_path = os.path.join(dir_path, i)
                #['H_###', 'S##', 'L###', 'R#', '###', '###.fastq.gz']                            
                isplit = i.rsplit("_", -1)
                isplit[0] = h_name
                if isplit[5].split(".")[0] == min(uniq_ids):
                    isplit[5] = "1.fastq.gz"
                else:
                    isplit[5]= "2.fastq.gz"
                new_name = "_".join([isplit[0], isplit[3], isplit[2], isplit[5]])
                try:
                  os.symlink(file_path, new_name) 
                except FileExistsError as e:
                    print(e)