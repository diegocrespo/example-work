import pandas as pd
import sqlite3

def query1():
    """ Returns # of distinct annotations per maxpath """    
    query = """
    SELECT maxpaths.maxpath_id AS maxpath_id, COUNT(maxpaths.annottype_name) AS Counts
    FROM
    (SELECT DISTINCT(annottype.name) as annottype_name, maxpath.id as maxpath_id
    FROM maxpath
    INNER JOIN maxpathalign ON maxpath.id = maxpathalign.maxpath_id
    INNER JOIN maxpathannot ON maxpathalign.id = maxpathannot.maxpathalign_id
    INNER JOIN annot ON maxpathannot.annot_id = annot.id
    INNER JOIN annottype ON annot.annot_type_id = annottype.id) as maxpaths
    GROUP BY maxpaths.maxpath_id
    ORDER BY COUNTS DESC
    """    
    conn = sqlite3.connect('smRNASeq.db')
    df = pd.read_sql_query(query,conn,index_col='maxpath_id' )
    df.to_csv('num_of_unique_annotations_by_maxpath.csv', sep=',',index=True)
    return df

def query2():
    """ Returns what biotypes are associated with each cluster """
    
    query = """SELECT cluster.name as cluster_name,
    -- Sum  annottype put in a seperate column,
    -- append 0 if no annottype present in column
    SUM(CASE WHEN annottype.name = "protein_coding" THEN 1 ELSE 0 END) protein_coding,
    SUM(CASE WHEN annottype.name = "antisense_RNA" THEN 1 ELSE 0 END) antisense_RNA,
    SUM(CASE WHEN annottype.name = "sense_overlapping" THEN 1 ELSE 0 END) sense_overlapping,
    SUM(CASE WHEN annottype.name = "pre-miRNA" THEN 1 ELSE 0 END) premiRNA,
    SUM(CASE WHEN annottype.name = "miRNA" THEN 1 ELSE 0 END) miRNA,
    SUM(CASE WHEN annottype.name = "lincRNA" THEN 1 ELSE 0 END) lincRNA,
    SUM(CASE WHEN annottype.name = "unprocessed_pseudogene" THEN 1 ELSE 0 END) unprocessed_pseudogene,
    SUM(CASE WHEN annottype.name = "processed_transcript" THEN 1 ELSE 0 END) processed_transcript,    
    SUM(CASE WHEN annottype.name = "snoRNA" THEN 1 ELSE 0 END) snoRNA,
    SUM(CASE WHEN annottype.name = "processed_pseudogene" THEN 1 ELSE 0 END) processed_psuedogene,
    SUM(CASE WHEN annottype.name = "transcribed_unprocessed_pseudogene" THEN 1 ELSE 0 END) transcribed_unprocessed_pseudogene,
    SUM(CASE WHEN annottype.name = "snRNA" THEN 1 ELSE 0 END) snRNA,
    SUM(CASE WHEN annottype.name = "transcribed_processed_pseudogene" THEN 1 ELSE 0 END) transcribed_processed_pseudogene,
    SUM(CASE WHEN annottype.name = "Mt_tRNA" THEN 1 ELSE 0 END) Mt_tRNA,
    SUM(CASE WHEN annottype.name = "transcribed_unitary_pseudogene" THEN 1 ELSE 0 END) transcribed_unitary_pseudogene,
    SUM(CASE WHEN annottype.name = "Mt_rRNA" THEN 1 ELSE 0 END) Mt_rRNA,
    SUM(CASE WHEN annottype.name = "misc_RNA" THEN 1 ELSE 0 END) misc_RNA,
    SUM(CASE WHEN annottype.name = "TEC" THEN 1 ELSE 0 END) TEC,
    SUM(CASE WHEN annottype.name = "sense_intronic" THEN 1 ELSE 0 END) sense_intronic,
    SUM(CASE WHEN annottype.name = "non_coding" THEN 1 ELSE 0 END) non_coding,
    SUM(CASE WHEN annottype.name = "rRNA" THEN 1 ELSE 0 END) rRNA,
    SUM(CASE WHEN annottype.name = "bidirectional_promoter_lncRNA" THEN 1 ELSE 0 END) bidirectional_promoter_lncRNA,
    SUM(CASE WHEN annottype.name = "IG_V_pseudogene" THEN 1 ELSE 0 END) IG_V_pseudogene,
    SUM(CASE WHEN annottype.name = "unitary_pseudogene" THEN 1 ELSE 0 END) unitary_pseudogene,
    SUM(CASE WHEN annottype.name = "IG_V_gene" THEN 1 ELSE 0 END) IG_V_gene,
    SUM(CASE WHEN annottype.name = "ribozyme" THEN 1 ELSE 0 END) ribozyme,
    SUM(CASE WHEN annottype.name = "macro_lncRNA" THEN 1 ELSE 0 END) macro_lncRNA,
    SUM(CASE WHEN annottype.name = "scaRNA" THEN 1 ELSE 0 END) scaRNA,
    SUM(CASE WHEN annottype.name = "3prime_overlapping_ncRNA" THEN 1 ELSE 0 END) threeprime_overlapping_ncRNA,
    SUM(CASE WHEN annottype.name = "polymorphic_pseudogene" THEN 1 ELSE 0 END) polymorphic_pseudogene,
    SUM(CASE WHEN annottype.name = "sRNA" THEN 1 ELSE 0 END) sRNA,
    SUM(CASE WHEN annottype.name = "TR_V_gene" THEN 1 ELSE 0 END) TR_V_gene,
    SUM(CASE WHEN annottype.name = "TR_C_gene" THEN 1 ELSE 0 END) TR_C_gene,
    SUM(CASE WHEN annottype.name = "TR_V_pseudogene" THEN 1 ELSE 0 END) TR_V_pseudogene,
    SUM(CASE WHEN annottype.name = "scRNA" THEN 1 ELSE 0 END) scRNA,    
    SUM(CASE WHEN annottype.name = "IG_C_pseudogene" THEN 1 ELSE 0 END) IG_C_pseudogene,
    SUM(CASE WHEN annottype.name = "IG_C_gene" THEN 1 ELSE 0 END) IG_C_gene,
    SUM(CASE WHEN annottype.name = "TR_J_gene" THEN 1 ELSE 0 END) TR_J_gene,
    SUM(CASE WHEN annottype.name = "vaultRNA" THEN 1 ELSE 0 END) vaultRNA,
    SUM(CASE WHEN annottype.name = "translated_processed_pseudogene" THEN 1 ELSE 0 END) translated_processed_pseudogene
    FROM annot INNER JOIN annottype ON annot.annot_type_id = annottype.id
    INNER JOIN maxpathannot ON maxpathannot.annot_id = annot.id
    INNER JOIN maxpathalign ON maxpathalign.id = maxpathannot.maxpathalign_id
    INNER JOIN maxpath ON maxpath.id = maxpathalign.maxpath_id
    INNER JOIN graph ON graph.id = maxpath.graph_id
    INNER JOIN cluster ON cluster.id = graph.cluster_id
    GROUP BY cluster.name"""

    conn = sqlite3.connect('smRNASeq.db')
    df = pd.read_sql_query(query,conn,index_col='cluster_name' )
    df.to_csv('biotypes.csv', sep=',',index=True)
    return df


def query3():
    """ returns clusters with disjoint maxpaths """
    query = """
    SELECT result.cluster_id, result.cluster_name, result.maxpath, result.annot_id
    FROM
    (SELECT cluster.id AS cluster_id, cluster.name AS cluster_name, maxpath.path AS maxpath, annot.annot_id as annot_id
    FROM cluster
    INNER JOIN graph ON cluster.id = graph.cluster_id
    INNER JOIN maxpath ON graph.id = maxpath.graph_id
    INNER JOIN maxpathalign ON maxpath.id = maxpathalign.maxpath_id
    INNER JOIN maxpathannot ON maxpathalign.id = maxpathannot.maxpathalign_id
    INNER JOIN annot ON maxpathannot.annot_id = annot.id) AS result
    INNER JOIN
    -- Filter out rows with only one annotation
    (SELECT cluster.id AS cluster_id,count(annot.annot_id) as counts
    FROM cluster
    INNER JOIN graph ON cluster.id = graph.cluster_id
    INNER JOIN maxpath ON graph.id = maxpath.graph_id
    INNER JOIN maxpathalign ON maxpath.id = maxpathalign.maxpath_id
    INNER JOIN maxpathannot ON maxpathalign.id = maxpathannot.maxpathalign_id
    INNER JOIN annot ON maxpathannot.annot_id = annot.id
    GROUP BY cluster.id
    HAVING counts >1) AS FILTER
    WHERE result.cluster_id = FILTER.cluster_id""" 
    conn = sqlite3.connect('smRNASeq.db')
    df = pd.read_sql_query(query,conn)
    df.to_csv('disjoint_maxpaths.csv', sep=',',index=True)
    return df


def query4():
    """ Returns clusters with no annotated maxpaths """
    query = """
  SELECT cluster.id AS clusterid, cluster.name AS 'cluster_name', maxpath.seq, maxpathalign.pos
  FROM maxpath INNER JOIN graph ON maxpath.graph_id = graph.id
  INNER JOIN cluster ON cluster.id = graph.cluster_id
  LEFT JOIN maxpathalign ON maxpath.id = maxpathalign.maxpath_id
  LEFT JOIN maxpathannot ON maxpathalign.id = maxpathannot.maxpathalign_id
  LEFT JOIN annot ON maxpathannot.annot_id = annot.id
  WHERE maxpathannot.annot_id IS Null
  -- GROUP by cluster id to get distinct clusters
  GROUP BY cluster.id"""

    conn = sqlite3.connect('smRNASeq.db')
    df = pd.read_sql_query(query,conn,index_col='clusterid' )
    subset_df = df['cluster_name']
    subset_df.to_csv('list_cluster_no_maxpath.csv', sep=',', index=False)
    df.to_csv('no_annotated_maxpaths.csv', sep=',',index=True)
    return df


def query5():
    """ Returns # of unique annottation types per maxpath """    
    query = """
    SELECT DISTINCT(cluster.id) AS cluster_id, annottype.name
    FROM cluster
    INNER JOIN graph on cluster.id = graph.cluster_id
    INNER JOIN maxpath on graph.id = maxpath.graph_id
    INNER JOIN maxpathalign on maxpath.id = maxpathalign.maxpath_id
    INNER JOIN maxpathannot on maxpathalign.id = maxpathannot.maxpathalign_id
    INNER JOIN annot on maxpathannot.annot_id = annot.id
    INNER JOIN annottype on annot.annot_type_id = annottype.id
    """

    conn = sqlite3.connect('smRNASeq.db')
    df = pd.read_sql_query(query,conn,index_col='cluster_id' )
    df.to_csv('maxpath_annotation_types.csv', sep=',',index=True)
    return df
